package br.goncalves.concrete.domain;

import com.squareup.otto.Subscribe;

import br.goncalves.concrete.model.entities.ConnectionError;
import br.goncalves.concrete.model.entities.GenericError;
import br.goncalves.concrete.model.entities.HttpError;

/**
 * Created by Halyson on 21/03/15.
 */
public interface RequestError {
    @Subscribe
    void onRequestErrorConnection(ConnectionError connectionError);

    @Subscribe
    void onRequestErrorHttp(HttpError httpError);

    @Subscribe
    void onRequestErrorGeneric(GenericError genericError);
}
