package br.goncalves.concrete.interfaces;

/**
 * Created by Halyson on 16/01/15.
 */
public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
