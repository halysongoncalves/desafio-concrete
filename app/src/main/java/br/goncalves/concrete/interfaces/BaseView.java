package br.goncalves.concrete.interfaces;

public interface BaseView {
    void loadToolbar();

    void loadDrawerMenu();
}
