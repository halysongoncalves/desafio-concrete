package br.goncalves.concrete;

import android.app.Application;
import android.content.Context;


public class Concrete extends Application {
    private static Concrete application;

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;
    }


    public static Concrete getApplication() {
        return application;
    }

    public static Context getContext() {
        return application.getApplicationContext();
    }
}
