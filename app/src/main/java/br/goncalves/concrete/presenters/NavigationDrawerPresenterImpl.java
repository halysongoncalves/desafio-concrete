package br.goncalves.concrete.presenters;

import com.squareup.otto.Subscribe;

import br.goncalves.concrete.common.BusProvider;
import br.goncalves.concrete.domain.GetProfileController;
import br.goncalves.concrete.interfaces.NavigationDrawerPresenter;
import br.goncalves.concrete.interfaces.NavigationDrawerView;
import br.goncalves.concrete.model.entities.Profile;
import br.goncalves.concrete.model.rest.WebServiceManagerImpl;

public class NavigationDrawerPresenterImpl implements NavigationDrawerPresenter {
    private final NavigationDrawerView navigationDrawerView;

    public NavigationDrawerPresenterImpl(NavigationDrawerView navigationDrawerView) {
        BusProvider.getUIBusInstance().register(this);
        this.navigationDrawerView = navigationDrawerView;
    }

    @Override
    public void loadProfile() {
        new GetProfileController(WebServiceManagerImpl.getInstance(),
                BusProvider.getUIBusInstance())
                .requestProfile();
    }

    @Subscribe
    public void onPopulateProfileUserReceived(Profile profile) {
        navigationDrawerView.showPictureProfile(profile.getAvatar_url());
        navigationDrawerView.showNameProfile(profile.getName());

    }

    @Override
    public void onDestroy() {
        BusProvider.getUIBusInstance().unregister(this);
    }
}
