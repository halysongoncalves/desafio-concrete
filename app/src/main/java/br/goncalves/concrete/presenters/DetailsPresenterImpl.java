package br.goncalves.concrete.presenters;

import com.squareup.otto.Subscribe;

import br.goncalves.concrete.Concrete;
import br.goncalves.concrete.R;
import br.goncalves.concrete.common.BusProvider;
import br.goncalves.concrete.domain.GetShotController;
import br.goncalves.concrete.interfaces.DetailView;
import br.goncalves.concrete.interfaces.DetailsPresenter;
import br.goncalves.concrete.model.entities.ConnectionError;
import br.goncalves.concrete.model.entities.GenericError;
import br.goncalves.concrete.model.entities.HttpError;
import br.goncalves.concrete.model.entities.Shot;
import br.goncalves.concrete.model.rest.WebServiceManagerImpl;

public class DetailsPresenterImpl implements DetailsPresenter {
    private final DetailView detailView;

    public DetailsPresenterImpl(DetailView detailView) {
        BusProvider.getUIBusInstance().register(this);
        this.detailView = detailView;
    }

    @Override
    public void loadShot(int shotId) {
        new GetShotController(WebServiceManagerImpl.getInstance(),
                BusProvider.getUIBusInstance())
                .requestShot(shotId);
    }

    @Subscribe
    public void onPopulateShot(Shot shot) {
        detailView.hideLoading();
        detailView.showObservableScrollView();
        detailView.showShot(shot);
    }

    @Subscribe
    public void onConnectionError(ConnectionError connectionError) {
        detailView.showError(Concrete.getContext().getString(R.string.fragment_home_dialog_text_title_error),
                Concrete.getContext().getString(R.string.fragment_home_dialog_text_messae_connection_error));
    }

    @Subscribe
    public void onHttpError(HttpError httpError) {
        detailView.showError(Concrete.getContext().getString(R.string.fragment_home_dialog_text_title_error),
                Concrete.getContext().getString(R.string.fragment_home_dialog_text_messae_http_error));
    }

    @Subscribe
    public void onGenericError(GenericError genericError) {
        detailView.showError(Concrete.getContext().getString(R.string.fragment_home_dialog_text_title_error),
                Concrete.getContext().getString(R.string.fragment_home_dialog_text_messae_generic_error));
    }

    @Override
    public void onDestroy() {
        BusProvider.getUIBusInstance().unregister(this);
    }

}
