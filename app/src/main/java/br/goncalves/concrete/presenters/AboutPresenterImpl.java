package br.goncalves.concrete.presenters;

import com.squareup.otto.Subscribe;

import br.goncalves.concrete.common.BusProvider;
import br.goncalves.concrete.domain.GetProfileController;
import br.goncalves.concrete.interfaces.AboutPresenter;
import br.goncalves.concrete.interfaces.AboutView;
import br.goncalves.concrete.model.entities.Profile;
import br.goncalves.concrete.model.rest.WebServiceManagerImpl;

public class AboutPresenterImpl implements AboutPresenter {
    private final AboutView aboutView;

    public AboutPresenterImpl(AboutView aboutView) {
        BusProvider.getUIBusInstance().register(this);
        this.aboutView = aboutView;
    }

    @Override
    public void loadProfile() {
        new GetProfileController(WebServiceManagerImpl.getInstance(),
                BusProvider.getUIBusInstance())
                .requestProfile();
    }

    @Subscribe
    public void onPopulateProfileUserReceived(Profile profile) {
        aboutView.showPictureProfile(profile.getAvatar_url());
        aboutView.showNameProfile(profile.getName());
        aboutView.showLocation(profile.getLocation());
    }

    @Override
    public void onDestroy() {
        BusProvider.getUIBusInstance().unregister(this);
    }
}
